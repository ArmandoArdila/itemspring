package com.formacionbdi.springboo.app.item.models.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.formacionbdi.springboo.app.item.models.Item;
import com.formacionbdi.springboo.app.item.models.ItemService;
import com.formacionbdi.springboo.app.item.models.Producto;
@Service
public class ItemServiceImpl implements ItemService {
	@Autowired
	private RestTemplate clienteRest;
	@Override
	public List<Item> findAll() {
		List<Producto> productos = Arrays.asList(clienteRest.getForObject("http://localhost:8001/listaProductos", Producto[].class));
		return productos.stream().map(p -> new Item(p, 1)).collect(Collectors.toList());
	}

	@Override
	public Item findById(Long id, Integer cantidad) {
		Map<String, String> queryParams = new HashMap<String,String>();
		queryParams.put("id", id.toString());
		Producto p = clienteRest.getForObject("/getUsuarios", Producto.class, queryParams);
		return new Item(p, cantidad);
	}

}
